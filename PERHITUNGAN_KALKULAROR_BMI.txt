import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'BMIModel.dart';
import 'ResultScreen.dart';

class BMICalculatorScreen extends StatefulWidget {
  @override
  _BMICalculatorScreenState createState() => _BMICalculatorScreenState();
}

class _BMICalculatorScreenState extends State<BMICalculatorScreen> {
  double _heightOfUser = 100.0;
  double _weightOfUser = 40.0;

  double _bmi = 0;

  BMIModel _bmiModel;

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: new AppBar(
        leading: new Icon(Icons.view_list),
        title: new Text('Menghitung BMI'),
        centerTitle: true,
        backgroundColor: Colors.orange,
        actions: <Widget>[
          new IconButton(
            icon: Icon(Icons.person_outline),
            onPressed: (){
              Navigator.pushNamed(context, '/Profil');
            },
          )
        ],
      ),

      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.all(32),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 200,
                width: 200,
                child: SvgPicture.asset("assets/images/bmi.svg", fit: BoxFit.contain,),
              ),
              SizedBox(
                height: 8,
              ),
              Text("Perhitungan BMI", style: TextStyle(color: Colors.yellowAccent[700], fontSize: 34, fontWeight: FontWeight.w700),),
              Text("mulailah hidup sehat dengan pola makan bergizi dan seimbang", style: TextStyle(color: Colors.grey, fontSize: 15, fontWeight: FontWeight.w400),),
              SizedBox(
                height: 32,
              ),

              Text("Tinggi Badan (cm)", style: TextStyle(color: Colors.grey, fontSize: 24, fontWeight: FontWeight.w400),),

              Container(
                padding: EdgeInsets.only(left: 16, right: 16),
                child: Slider(
                  min: 80.0,
                  max: 250.0,
                  onChanged: (height){
                    setState(() {
                      _heightOfUser = height;
                    });
                  },
                  value: _heightOfUser,
                  divisions: 100,
                  activeColor: Colors.purpleAccent,
                  label: "$_heightOfUser",
                ),
              ),

              Text("$_heightOfUser cm", style: TextStyle(color: Colors.deepPurple, fontSize: 18, fontWeight: FontWeight.w900),),

              SizedBox(height: 24,),

              Text("Berat Badan (kg)", style: TextStyle(color: Colors.grey, fontSize: 24, fontWeight: FontWeight.w400),),

              Container(
                padding: EdgeInsets.only(left: 16, right: 16),
                child: Slider(
                  min: 30.0,
                  max: 120.0,
                  onChanged: (height){
                    setState(() {
                      _weightOfUser = height;
                    });
                  },
                  value: _weightOfUser,
                  divisions: 100,
                  activeColor: Colors.pink,
                  label: "$_weightOfUser",
                ),
              ),

              Text("$_weightOfUser kg", style: TextStyle(color: Colors.purpleAccent, fontSize: 18, fontWeight: FontWeight.w900),),

              SizedBox(height: 16,),

              Container(
                child: FlatButton.icon(
                  onPressed: (){
                    setState(() {
                      _bmi = _weightOfUser/((_heightOfUser/100)*(_heightOfUser/100));

                      if(_bmi >= 18.5 && _bmi <= 25){
                        _bmiModel = BMIModel(bmi: _bmi, isNormal: true, comments: "Selamat Badanmu Sehat");
                      }else if(_bmi < 18.5){
                        _bmiModel = BMIModel(bmi: _bmi, isNormal: false, comments: "Kamu Terlalu Kurus. Makan Yang Banyak Ya");
                      }else if(_bmi > 25 && _bmi <= 30){
                        _bmiModel = BMIModel(bmi: _bmi, isNormal: false, comments: "Kamu Kegendutan. Ayo Mulai Olahraga Teratur");
                      }else{
                        _bmiModel = BMIModel(bmi: _bmi, isNormal: false, comments: "WOW Kamu Gendut Sekali. Ayo Diet");
                      }
                    });

                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => ResultScreen(bmiModel: _bmiModel,)
                    ));
                  },
                  icon: Icon(Icons.play_arrow, color: Colors.white,),
                  label: Text("CALCULATE"),
                  textColor: Colors.white,
                  color: Colors.purple,

                ),
                width: double.infinity,
                padding: EdgeInsets.only(left: 16, right: 16),
              )

            ],
          ),
        ),
      ),
    );
  }
}